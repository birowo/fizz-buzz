package main

import (
	"os"
	"testing"
)

func itoaln(val int, slc []byte, i int) int {
	slc[i] = '\n'
	j := i + 1
	for {
		slc[j] = byte(val%10) + '0' //least significant digit comes first, so it must be reversed
		val /= 10
		if val == 0 {
			k := j + 1
			for ; i < j; i, j = i+1, j-1 { //reverse digits
				slc[i], slc[j] = slc[j], slc[i]
			}
			return k
		}
		j++
	}
}

const sz = 99999

func BenchmarkNoModWithAlloc(b *testing.B) {
	//no modulo trick, with alloc
	slc := make([]byte, sz)
	fizzBuzz := []byte("FizzBuzz\n")
	fizzzBuzzz := []byte("Fizz\nBuzz\n")
	buzzzFizzz := []byte("Buzz\nFizz\n")
	fizz := []byte("Fizz\n")
	l2 := 0
	for i := 1; i < 17071; i += 15 {
		l2 = itoaln(i, slc, l2)          //1
		l2 = itoaln(i+1, slc, l2)        //2
		l2 += copy(slc[l2:], fizz)       //3
		l2 = itoaln(i+3, slc, l2)        //4
		l2 += copy(slc[l2:], buzzzFizzz) //5, 6
		l2 = itoaln(i+6, slc, l2)        //7
		l2 = itoaln(i+7, slc, l2)        //8
		l2 += copy(slc[l2:], fizzzBuzzz) //9, 10
		l2 = itoaln(i+10, slc, l2)       //11
		l2 += copy(slc[l2:], fizz)       //12
		l2 = itoaln(i+12, slc, l2)       //13
		l2 = itoaln(i+13, slc, l2)       //14
		l2 += copy(slc[l2:], fizzBuzz)   //15
	}
	os.Stdout.Write(slc[:l2])
}
func BenchmarkWithAlloc(b *testing.B) {
	//with alloc
	slc := make([]byte, sz)
	fizzBuzz := []byte("FizzBuzz\n")
	fizz := []byte("Fizz\n")
	buzz := []byte("Buzz\n")
	l1 := 0
	for i := 1; i < 17071; i++ {
		if (i%3 == 0) && (i%5 == 0) {
			l1 += copy(slc[l1:], fizzBuzz)
		} else if i%3 == 0 {
			l1 += copy(slc[l1:], fizz)
		} else if i%5 == 0 {
			l1 += copy(slc[l1:], buzz)
		} else {
			l1 = itoaln(i, slc, l1)
		}
	}
	os.Stdout.Write(slc[:l1])
}
func BenchmarkNoAlloc(b *testing.B) {
	//no alloc
	fizzBuzz := []byte("FizzBuzz\n")
	fizz := []byte("Fizz\n")
	buzz := []byte("Buzz\n")
	slc := []byte{'\n', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	for i := 1; i < 17071; i++ {
		if (i%3 == 0) && (i%5 == 0) {
			os.Stdout.Write(fizzBuzz)
		} else if i%3 == 0 {
			os.Stdout.Write(fizz)
		} else if i%5 == 0 {
			os.Stdout.Write(buzz)
		} else {
			os.Stdout.Write(slc[:itoaln(i, slc, 0)])
		}
	}
}
func main() {
}
